package com.gslab.mongo3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gslab.mongo3.exception.RoleNotFoundException;
import com.gslab.mongo3.model.Role;
import com.gslab.mongo3.repository.RoleRepository;

@Service
public class RoleService {
    
	@Autowired
	RoleRepository roleRepository;
	
	public List<Role> getAllRole() {
		
		return (List<Role>) roleRepository.findAll();
		
	}
	
	public Role addRole(Role role) {
		return roleRepository.save(role);
	}
	
	public Role getRole(String id) {
		//System.out.println("finding by id "+ string);
		return roleRepository.findByid(id).orElseThrow(() -> new RoleNotFoundException(id));
	}
	
	public Role updateRole(Role newRole, String id){
		
		Role updatedRole =  roleRepository.findByid(id).map(role -> {
			role.setRoleName(newRole.getRoleName());
			return roleRepository.save(role);
		}).orElseThrow(()-> new RoleNotFoundException(id));
		
		return updatedRole;		
	}

	public void deleteRole(String id) {
		// TODO Auto-generated method stub
		roleRepository.deleteByid(id);
	}
	
}
