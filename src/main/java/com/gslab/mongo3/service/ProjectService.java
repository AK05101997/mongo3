package com.gslab.mongo3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gslab.mongo3.exception.ProjectNotFoundException;
import com.gslab.mongo3.model.Project;
import com.gslab.mongo3.repository.ProjectRepository;

@Service
public class ProjectService {
    
	@Autowired
	ProjectRepository projectRepository;
	
	public List<Project> getAllProject() {
		
		return (List<Project>) projectRepository.findAll();
		
	}
	
	public Project addProject(Project project) {
		return projectRepository.save(project);
	}
	
	public Project getProject(String id) {
		//System.out.println("finding by id "+ string);
		return projectRepository.findByid(id).orElseThrow(() -> new ProjectNotFoundException(id));
	}
	
	public Project updateProject(Project newProject, String id){
		
		Project updatedProject =  projectRepository.findByid(id).map(project -> {
			project.setProjectName(newProject.getProjectName());
			return projectRepository.save(project);
		}).orElseThrow(()-> new ProjectNotFoundException(id));
		
		return updatedProject;		
	}

	public void deleteProject(String id) {
		// TODO Auto-generated method stub
		projectRepository.deleteByid(id);
	}
	
}
