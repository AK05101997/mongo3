package com.gslab.mongo3.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gslab.mongo3.assembler.ProjectResourceAssembler;
import com.gslab.mongo3.model.Project;
import com.gslab.mongo3.service.ProjectService;

@RestController
@RequestMapping("/mongo-api")
public class ProjectController {

	@Autowired 
	ProjectService projectService;
	
	@Autowired
	ProjectResourceAssembler projectAssembler;
	
	@GetMapping("/projects")
	public Resources<Resource<Project>> getAllProject() {
	   //System.out.println("getting all project");
		List<Project> project = projectService.getAllProject();
		
		List<Resource<Project>> projects = project.stream()
				.map(projectAssembler::toResource)
				.collect(Collectors.toList());
		//System.out.println("Going to get all project");
		return new Resources<>(projects, 
				linkTo(methodOn(ProjectController.class).getAllProject()).withSelfRel());
	}
	
	@PostMapping("/projects")
	public ResponseEntity<?> newProject(@RequestBody Project project) throws URISyntaxException {
		
		Project savedProject = projectService.addProject(project);
		Resource<Project> resource = projectAssembler.toResource(savedProject);
		   
		return ResponseEntity.created(new URI(resource.getId().expand(savedProject.getId()).getHref())).body(resource);
		
	}
	
	@GetMapping("/projects/{id}")
	public Resource<Project> getProject(@PathVariable String id) {
		//System.out.println("in getting project with id"+ id);
		Project project = projectService.getProject(id);
		//System.out.println("project id: "+project.get_id()+"project name  :"+ project.getProjectname());
		return projectAssembler.toResource(project);
	}

	@PutMapping("/projects/{id}")
	ResponseEntity<?> replaceProject(@RequestBody Project newProject, @PathVariable String id) throws URISyntaxException {

		Project updatedProject = projectService.updateProject(newProject, id);

		Resource<Project> resource = projectAssembler.toResource(updatedProject);

		return ResponseEntity
			.created(new URI(resource.getId().expand(updatedProject.getId()).getHref()))
			.body(resource);
	}
	
	@DeleteMapping("/projects/{id}")
	ResponseEntity<?> deleteProject(@PathVariable String id) {

		projectService.deleteProject(id);

		return ResponseEntity.noContent().build();
	}
}
