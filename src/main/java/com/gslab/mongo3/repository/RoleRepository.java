package com.gslab.mongo3.repository;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gslab.mongo3.model.Role;

@Repository
public interface RoleRepository extends MongoRepository<Role, ObjectId> {

	public Optional<Role> findByid(String id);
	public void deleteByid(String id);
}
